import UsersRepository from "../../dal/users/users.repository.ts";
import { User } from "../../types/users.interface.ts";
import { Service } from "typedi";

@Service()
class UsersService {
  constructor(private usersRepository: UsersRepository) {}

  getUserByEmail = (email: string): Promise<User> => {
    return this.usersRepository.getByEmail(email);
  };

  createAUser = (user: User) => {
    return this.usersRepository.createAUser(user);
  };
}

export default UsersService;