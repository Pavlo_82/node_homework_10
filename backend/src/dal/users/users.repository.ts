import { User } from "../../types/users.interface.ts";
import { Service } from "typedi";
import {ITable} from '../../types/params.interface.ts';
import { FileDB } from '../../fileDb/fileDb.ts';
@Service()
class UsersRepository {

private table: ITable
  constructor(){
    const fileDBInstance = FileDB.getInstance();
    this.table = fileDBInstance.getTable('user');    
  }

  getByEmail = async (email: string): Promise<User> => {
    const users = await this.table.getAll()
    const user = users.find((user:User) => user.email === email);
    return user ? { ...user } : null;
  };

  createAUser = async(user: User): Promise<User> => {
    const newUser = await this.table.create(user)
    return newUser;
  };
}

export default UsersRepository;
