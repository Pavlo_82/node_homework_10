import {ITable} from '../types/params.interface.ts';
import Table from './Table.ts';

export class FileDB {
  private static instance: FileDB;
  private tables: { [key: string]: ITable };

  private constructor() {
    this.tables = {};
  }

  public static getInstance(): FileDB {
    if (!FileDB.instance) {
      FileDB.instance = new FileDB();
    }
    return FileDB.instance;
  }

  registerSchema(tableName: string, schema: any) {
    this.tables[tableName] = schema;   
    return this.tables;
  }

  getTable(tableName: string): any {
    const table = this.tables[tableName];    
    if (!table) {
      throw new Error(`Table ${tableName} is not registered.`);
    }
    
    return new Table(tableName, table);
  }
}
