import {FileDB} from "./fileDb.ts";
const newsSchema = {
  id: String,
  title: String,
  text: String,
  createDate: Date,
  updateDate: Date,
};

const usersSchema = {
  id: String,
  email: String,
  password: String
};

// const newspost = 'newspost';
// const user = 'user'
export const createTable = async (newspost:string) => {
  const registerSchema = await FileDB.getInstance();
  await registerSchema.registerSchema(newspost, newsSchema);
  const Table = await registerSchema.getTable(newspost);  
  return Table
};
export const createTableUser = async (user:string) => {
  const registerSchema = await FileDB.getInstance();
  await registerSchema.registerSchema(user, usersSchema);
  const Table = await registerSchema.getTable(user);
  return Table
};
